
FROM python:3-slim

WORKDIR	/usr/src/app
#zlozka, v containery, kde sa bude aplikacia nachadzat 

RUN pip install flask
# instalovanie python packagov

COPY . .
# kopirovanie aplikacie z prepozitara do containeru

CMD ["python", "app.py"]
